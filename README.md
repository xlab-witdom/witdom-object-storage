# WITDOM Object Storage - OpenStack Swift
This repository contains configuration files to build WITDOM's Object Storage component, OpenStack Swift, deployed as an all-in-one service. It is a fork of [ccollicutt/docker-swift-onlyone](https://github.com/ccollicutt/docker-swift-onlyone) and is extended to use either Keystone authentication middleware (default, allowing to access OpenStack Swift service with OpenStack Keystone's authentication tokens) or Swift's legacy authentication (with username and password configured with Swift, for ease of development).

## Building the project
A Makefile is used to build the project. Before running `make` however, you should decide on what kind of authentication you will be using with Swift.

### a) Swift with legacy authentication

Run `make auth_legacy` and follow the usage instructions for this kind of authentication below.

### b) Swift with Keystone authentication middleware

Run `make` (which is the same as running `make auth_keystone`, as this is configured as the default authentication). Note that in order to use this kind of authentication, it is **mandatory that Keystone is properly configured**, meaning that:

* a project (tenant) _service_ must exist in Keystone
* a user _swift_ must exist in Keystone and has _service_ project set as default project
* user _swift_ has admin role on the _service_ project
* service of type _object-store_ must exist in Keystone for Swift
* a public endpoint must exist for Swift service and point to the right URL.

An example script that configures all of the above of the above can be seen [here](https://bitbucket.org/xlab-witdom/witdom-core-iam/src/master/keystone/docker/conf/initialize.sh?at=master&fileviewer=file-view-default)

## Using Swift Object Storage

### a) Swift with legacy authentication
1. Obtain authentication token by providing credentials configured in Swift (use username _test:tester_ and password _testing_) - this is done by a GET request to _/auth/v1.0_ endpoint, where headers _X-Auth-User_ and _X-Auth-Key_ must be set.

    ````bash
    GET http://127.0.0.1:8080/auth/v1.0/
    X-Auth-User: test:tester
    X-Auth-Key: testing
    ````

    You should see a similar response:

    ````bash
    200: OK
    X-Storage-Url: http://127.0.0.1:8080/v1/AUTH_test
    X-Auth-Token: AUTH_tk8ee101f1eb5d432aa438aec6dd3e3190
    Content-Type: text/html; charset=UTF-8
    X-Storage-Token: AUTH_tk8ee101f1eb5d432aa438aec6dd3e3190
    Content-Length: 0
    X-Trans-Id: tx5d5454caa81044cb99f8d-00582c15a1
    Date: Wed, 16 Nov 2016 08:15:29 GMT
    ````

2. Remember _X-Auth-Token_ header from the previous response - you will have to pass it to all of your following requests to Swift service. From the response you can also see the endpoint to which you should address further requests to Swift's Object Storage API - it can be found under _X-Storage-Url_ response header. 
3. You can now use [Swift Object Storage API](http://developer.openstack.org/api-ref/object-storage/) with Swift account _AUTH_test_. Remember to add _X-Auth-token_ header to every request. The example below shows how to list Swift containers for this account. 

    ````bash
    GET http://127.0.0.1:8080/v1/AUTH_test
    X-Auth-Token: AUTH_tkcc6b6c638a7d40f8ab25b7207f7213e3
    ````

    As we didn't create any containers for this account yet, we should get an empty 204 No Content resonse.

>**Note:** If you get 401 Forbidden responses despite passing a previously obtained token in _X-Auth-Token_ header, it is possible that your token has expired. In this case, obtain a new one by repeating Step 1.
 
### b) Swift with Keystone authentication middleware
The steps here are actually similar to those in _Swift with legacy authentication_, except that we obtain authentication token from Keystone rather than Swift service itself. Moreover, beside sending authentication token in response, Keystone's response also indicates Swift's public endpoint and Swift account to use. 

1. Get authentication token from Keystone. Use username _swift_ and password _swiftpw_ if you are using Dockerized Keystone available [here](https://bitbucket.org/xlab-witdom/witdom-core-iam/src/master/keystone/docker).

    ````bash
    POST http://127.0.0.1:5000/v3/auth/tokens
    {
    "auth": {
        "identity": {
            "methods": [
                "password"
            ],
            "password": {
                "user": {
                    "name": "swift",
                    "domain": {
                        "name": "Default"
                    },
                    "password": "swiftpw"
                    }
                }
            }
        }
    }
    ````

    We get a response similar to the one below:

    ````bash
    201 Created
    X-Subject-Token: MIIIAAYJKoZIhvcNAQcCoIIH8TCCB+0CAQExDTALBglghkgBZQMEAgEwggZOBgkqhkiG9w0BBwGgggY-BIIGO3sidG9rZW4iOiB7Im1ldGhvZHMiOiBbInBhc3N3b3JkIl0sI...
    "catalog": [
          {
            "endpoints": [
              {
            "url": "http://172.17.0.1:8080/v1/AUTH_425e7874012e4059bdebcbfbfcd0f982",
            "region": "RegionOne",
            "interface": "public",
            "id": "4e96036dfe2545aca53cfac47ad9d968"
            },
              {
            "url": "http://172.17.0.1:8080/",
            "region": "RegionOne",
            "interface": "admin",
            "id": "cc77ffc10cca44858810beff8a3840c9"
            },
              {
            "url": "http://172.17.0.1:8080/v1/AUTH_425e7874012e4059bdebcbfbfcd0f982",
            "region": "RegionOne",
            "interface": "internal",
            "id": "cd3383c1f328417db0aa0c2c70c0661d"
            }
            ],
        "type": "object-store",
        "id": "3ce81715850b4eedb68f6bd05e7962b3",
        "name": "swift"
        }
    ],
    ````

2. In the following requests to Swift service you will have to pass the value of _X-Subject-Token_ from the previous response in the _X-Auth-Token_ header. You will address Swift's storage API at the public endpoint indicated in the previous response - in our example this is _http://172.17.0.1:8080/v1/AUTH_425e7874012e4059bdebcbfbfcd0f982_. It should also work if you replace this IP with _127.0.0.1_.

3. You can now use [Swift Object Storage API](http://developer.openstack.org/api-ref/object-storage/) with Swift account *AUTH_stringFromResponse*. Remember to add *X-Auth-Token* header to every request. The example below shows how to list Swift containers for this account. 

    ````bash
    GET http://127.0.0.1:8080/v1/AUTH_425e7874012e4059bdebcbfbfcd0f982
    X-Auth-Token: MIIIAAYJKoZIhvcNAQcCoIIH8TCCB+0CAQExDTALBglghkgBZQMEAgEwggZOBgkqhkiG9w0BBwGgggY-BIIGO3sidG9rZW4iOiB7Im1ldGhvZHMiOiBbInBhc3N3b3JkIl0sI...
    ````
    Again, you should get a _204 No content_ response as no containers exist for this account at this point. To create a container for the account, you can send a request similar to the one below:
    
    ````bash
    PUT http://127.0.0.1:8080/v1/AUTH_425e7874012e4059bdebcbfbfcd0f982/witdom-test-container
    Content-Type: application/json
    Content-Length: 0
    X-Auth-Token: MIIIAAYJKoZIhvcNAQcCoIIH8TCCB+0CAQExDTALBglghkgBZQMEAgEwggZOBgkqhkiG9w0BBwGgggY-BIIGO3sidG9rZW4iOiB7Im1ldGhvZHMiOiBbInBhc3N3b3JkIl0sI...
    X-Container-Meta-Author: WITDOM-Test-User
    ````
    
    The above request will create object container _witdom-test-container_, where you can store objects.

## Print Swift logs

Run `make logs` to output latest Swift logs.

## Log-in into Docker container running Swift

Run `make bash`

## Cleaning up

Run `make clean` to stop and remove the containers

## More examples

The documentation above is focused on how to properly authenticate with OpenStack Swift service in this project. For more examples and information regarding OpenStack Swift's Object Storage API please refer to [official documentation](http://developer.openstack.org/api-ref/object-storage/)