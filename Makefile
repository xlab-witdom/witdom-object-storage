
CONTAINER=swift
IMAGE=swift
DATA_IMAGE=busybox
MAPPED_PORT=8080
DOCKER_PORT=8080
KEYSTONE_ADMIN_PORT=35357

# Change to DOCKERFILE=Dockerfile_auth if you want to build with Keystone authentication
DOCKERFILE=Dockerfile

# Only needed when running Keystone instance in Docker container
#	for using Keystone authentication middleware (tokens)
#IP=$(shell ./get_docker_ip.sh) 
#IP="172.17.0.1"


# Only needed when using legacy authentication (for testing purposes)
SWIFT_USER_PASSWORD=testing

all: auth_keystone

auth_legacy: configure_legacy build run

auth_keystone: build run

redeploy: delete build run_swift

run: run_swift_data run_swift

run_swift_data:
	-docker run \
  	-v /srv \
	--name $(CONTAINER)-data \
    $(DATA_IMAGE)	

run_swift:
	docker run \
	--name $(CONTAINER) \
	--hostname $(CONTAINER) \
	-e "SWIFT_USER_PASSWORD=$(SWIFT_USER_PASSWORD)" \
	-d \
	-p $(MAPPED_PORT):$(DOCKER_PORT) \
	--volumes-from $(CONTAINER)-data \
	-t $(IMAGE) 

clean:
	docker rm -f $(CONTAINER)-data && \
	docker rm -f $(CONTAINER)

bash:
	docker exec -i -t $(CONTAINER) /bin/bash

# Copy swift configuration for legacy authentication system
configure_legacy:
	cp files/proxy-server-legacy.conf files/proxy-server.conf 

# Configure and copy swift configuration for Keystone authentication middleware
# Substitutes IP in configuration file		
#configure_keystone:
#	sed	-e "s/{port}/$(KEYSTONE_ADMIN_PORT)/" \
#		-e "s/{ip}/$(IP)/" \
#		files/proxy-server-keystone.conf.template > files/proxy-server.conf

build:
	docker build -t $(IMAGE) -f $(DOCKERFILE) .

logs:
	docker logs $(CONTAINER) 
