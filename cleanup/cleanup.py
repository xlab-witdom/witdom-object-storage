#! /usr/bin/python

import json
from urllib2 import urlopen, Request, URLError

"""
An interactive script for viewing or deleting the contents of Swift object storage (witdom-object-storage).
"""

# Config
URL = "http://localhost:3333"
API_PREFIX = "/v1/"
USER = "test:tester"
KEY = "testing"
ACCOUNT = "AUTH_test"
AUTH_URL = "/auth/v1.0"
AUTH_TOKEN = ""

# Helper functions.
def create_request(url):
    return Request(url, headers={"Accept": "application/json", "X-Auth-User": USER, "X-Auth-Key": KEY, "X-Auth-Token": AUTH_TOKEN})

def send_request(request, error_msg=""):
    try:
        response = urlopen(request)
        return response
    except URLError as e:
        print error_msg, e.reason
        exit()

def get_auth_token():
    request = create_request(URL + AUTH_URL)
    response = send_request(request, "Cannot get Swift auth token\nError:")
    return response.info().getheader('X-Auth-Token')

def get_containers():
    request = create_request(URL + API_PREFIX + ACCOUNT)
    return json.loads(send_request(request, "Cannot get containers.\nError:").read())

def list_objects(container):
    request = create_request(URL + API_PREFIX + ACCOUNT + "/" + container)
    return json.loads(send_request(request, "Cannot list objects for container " + container + ".\nError:").read())

def clear_objects(container):
    for object in list_objects(container):
        request = create_request(URL + API_PREFIX + ACCOUNT + "/" + container + "/" + object["name"])
        request.get_method = lambda: "DELETE"
        send_request(request, "Cannot delete object " + object["name"])
    
def delete_container(container):
    clear_objects(container)
    request = create_request(URL + API_PREFIX + ACCOUNT + "/" + container)
    request.get_method = lambda: "DELETE"
    send_request(request, "Cannot delete container " + container)

# Get auth token
AUTH_TOKEN = get_auth_token()
print "Swift auth token:", AUTH_TOKEN

# Loop - choose container, then choose action
while True:
    containers = get_containers()

    print "Containers:"
    for i, container in enumerate(containers):
        print i, container["name"]

    print "\nChoose container (enter the number next to the name of the container) or enter q to exit"

    user_in = raw_input()
    if user_in in "qQ":
        exit()

    index = -1
    try:
        index = int(user_in)     
    except Exception as e:
        print "Error: not a number\n", e

    if index >= len(containers) or index < 0:
        print "Enter one of the listed numbers."
        continue
    
    container = containers[index]["name"]
    print "Chosen container:", container
    print "Select an action: l - list objects in container, c - clear objects in container, d - clear and delete container"

    user_in = raw_input()

    if user_in in "lL":
        objects = list_objects(container)
        print "Objects in container:"
        for object in objects:
            print object["name"]

    elif user_in in "cC":
        clear_objects(container)
    elif user_in in "dD":
        delete_container(container)
    else:
        print "Invalid command"

    print("\n\n")
