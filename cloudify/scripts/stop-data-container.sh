#! /bin/bash -e

ctx logger info "Stopping and removing swift-data container"
docker stop swift-data && docker rm swift-data
ctx logger info "Cleanup of swift-data container successfully completed"
