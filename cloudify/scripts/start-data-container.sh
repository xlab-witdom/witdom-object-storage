#! /bin/bash -e

ctx logger info "Will run swift-data container now"
docker run -v /srv --name swift-data busybox
ctx logger info "Successfully ran swift-data container"
